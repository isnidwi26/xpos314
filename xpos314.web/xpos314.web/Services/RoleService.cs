﻿using System.Text;
using Newtonsoft.Json;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class RoleService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public RoleService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<TblRole>> GetAllData()
        {
            List<TblRole> data = new List<TblRole>();
            string apiRespone = await client.GetStringAsync(RouteAPI + "apiRole/GetDataRole");
            data = JsonConvert.DeserializeObject<List<TblRole>>(apiRespone)!;
            return data;
        }

		public async Task<VMResponse> Create(TblRole dataParam)
		{
			//Proses convert dari object ke string
			string json = JsonConvert.SerializeObject(dataParam);
			//Proses mengubah string menjadi json lalu dikirim sebagai Request Body
			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
			//Proses memanggil API dan mengirimkan Body
			var request = await client.PostAsync(RouteAPI + "apiRole/Save", content);

			if (request.IsSuccessStatusCode)
			{
				//proses membaca respon dari API
				var apiRespon = await request.Content.ReadAsStringAsync();
				//proses convert hasil respon dari API ke object
				respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
			}
			else
			{
				respon.Success = false;
				respon.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
			}
			return respon;
		}

		public async Task<bool> CheckRoleName(string nameRole)
		{
			string apiResponse = await client.GetStringAsync(RouteAPI + $"apiRole/CheckRoleName/{nameRole}");
			bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);

			return isExist;
		}

		public async Task<TblRole> GetDataById(int id)
		{
			TblRole data = new TblRole();
			string apiResponse = await client.GetStringAsync(RouteAPI + $"apiRole/GetDataById/{id}");
			data = JsonConvert.DeserializeObject<TblRole>(apiResponse)!;
			return data;
		}

		public async Task<VMResponse> Edit(TblRole dataParam)
		{
			//Proses convert dari object ke string
			string json = JsonConvert.SerializeObject(dataParam);
			//Proses mengubah string menjadi json lalu dikirim sebagai Request Body
			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
			//Proses memanggil API dan mengirimkan Body
			var request = await client.PutAsync(RouteAPI + "apiRole/Edit", content);

			if (request.IsSuccessStatusCode)
			{
				//proses membaca respon dari API
				var apiRespon = await request.Content.ReadAsStringAsync();
				//proses convert hasil respon dari API ke object
				respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
			}
			else
			{
				respon.Success = false;
				respon.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
			}
			return respon;
		}

        public async Task<VMResponse> Delete(int id, int createdBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiRole/Delete/{id}/{createdBy}");
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
            }
            return respon;
        }


    }
}
