﻿using Microsoft.AspNetCore.Mvc;
using xpos314.web.Models;

namespace xpos314.web.Controllers
{
    public class FriendController : Controller
    {
        private static List<Friend> listFriends = new List<Friend> 
        { 
            new Friend(){Id=1, Name="Firdha", address="Tangerang Selatan"},
            new Friend(){Id=2, Name="Anwar", address="Ragunan"},                
        };
        public IActionResult Index()
        {

           ViewBag.Friends = listFriends;
            return View(listFriends);
        }

        public IActionResult Create() 
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Friend friend)
        {
            listFriends.Add(friend);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            Friend friend = listFriends.Find(i => i.Id == id)!;
            return View(friend);
        }

        [HttpPost]
        public IActionResult Edit(Friend friend)
        {
            Friend editfriend = listFriends.Find(i => i.Id == friend.Id)!;
            int indeks = listFriends.IndexOf(editfriend);
            if(indeks > -1)
            {
                listFriends[indeks].Id = friend.Id;
                listFriends[indeks].Name = friend.Name;
                listFriends[indeks].address = friend.address;
            }
            return RedirectToAction("Index");
        }

        public IActionResult Detail(int id)
        {
            Friend friend = listFriends.Find(i => i.Id == id)!;
            return View(friend);
        }

        /*public IActionResult Delete(int id)
        {
            Friend friend = listFriends.Find(i => i.Id == id)!;
            return View(friend);
        }

        [HttpPost]
        public IActionResult Delete(string id)
        {
            Friend deletefriend = listFriends.Find(i => i.Id == int.Parse(id))!;
            listFriends.Remove(deletefriend);
            return RedirectToAction("index");
        }*/

        [HttpGet]
        [HttpPost]
        public IActionResult Delete(int id)
        {
            Friend deleteFriend = listFriends.Find(i => i.Id == id)!;
            if (HttpContext.Request.Method == "POST")
            {
                listFriends.Remove(deleteFriend);
                return RedirectToAction("Index");
            }
            else
                return View(deleteFriend);
        }
    }
}
