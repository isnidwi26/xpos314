﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCustomerController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private VMResponse response = new VMResponse();
        private int IdUser = 1;

        public apiCustomerController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMTblCustomer> GetAllData()
        {
            List<VMTblCustomer> data = (from c in db.TblCustomers
                                        join r in db.TblRoles on c.IdRole equals r.Id
                                        where c.IsDelete == false
                                        select new VMTblCustomer
                                        {
                                            Id = c.Id,
                                            NameCustomer = c.NameCustomer,
                                            Email = c.Email,
                                            Password = c.Password,
                                            Address = c.Address,
                                            Phone = c.Phone,
                                            CreateDate = c.CreateDate,

                                            IdRole = r.Id,
                                            RoleName = r.RoleName
                                        }).ToList();

            return data;
        }

        [HttpGet("GetDataByIdCustomer/{id}")]
        public VMTblCustomer GetDataCustomerById(int id)
        {
            VMTblCustomer data = (from c in db.TblCustomers
                                        join r in db.TblRoles on c.IdRole equals r.Id
                                        where c.IsDelete == false && c.Id == id
                                        select new VMTblCustomer
                                        {
                                            Id = c.Id,
                                            NameCustomer = c.NameCustomer,
                                            Email = c.Email,
                                            Password = c.Password,
                                            Address = c.Address,
                                            Phone = c.Phone,
                                            CreateBy = c.CreateBy,
                                            CreateDate = c.CreateDate,
                                            UpdateBy = c.UpdateBy,
                                            UpdateDate = c.UpdateDate,

                                            IdRole = r.Id,
                                            RoleName = r.RoleName

                                        }).FirstOrDefault()!;
            return data;
        }

        [HttpGet("GetDataByIdRole")]
        public List<VMTblCustomer> GetDataByIdRole(int id)
        {
            List<VMTblCustomer> data = (from c in db.TblCustomers
                                        join r in db.TblRoles on c.IdRole equals r.Id
                                        where c.IsDelete == false && r.Id == id
                                        select new VMTblCustomer
                                        {
                                            Id = c.Id,
                                            NameCustomer = c.NameCustomer,
                                            Email = c.Email,
                                            Password = c.Password,
                                            Address = c.Address,
                                            Phone = c.Phone,

                                            IdRole = r.Id,
                                            RoleName = r.RoleName
                                        }).ToList();
            return data;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblCustomer data)
        {
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                response.Message = "Data success saved";
            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = "Failed saved " + e.Message;
            }
            return response;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblCustomer data)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == data.Id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.NameCustomer = data.NameCustomer;
                dt.Email = data.Email;
                dt.Password = data.Password;
                dt.Address = data.Address;
                dt.Phone = data.Phone;
                dt.IdRole = data.IdRole;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    response.Message = "Data success updates";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Update failed " + e.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    response.Message = $"Data {dt.NameCustomer} success deleted";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Delete Failed " + e.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }

    }
}
