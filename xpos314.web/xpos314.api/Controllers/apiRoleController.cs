﻿using System.Drawing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiRoleController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private VMResponse response = new VMResponse();
        private int IdUser = 1;

        public apiRoleController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetDataRole")]
        public List<TblRole> GetDataRole()
        {
            List<TblRole> data = db.TblRoles.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public TblRole DataById(int id)
        {
            TblRole result = new TblRole();
            result = db.TblRoles.Where(a => a.Id == id).FirstOrDefault()!;
            return result;
        }

		[HttpGet("CheckRoleName/{name}")]
		public bool CheckName(string name)
		{
			TblRole data = db.TblRoles.Where(a => a.RoleName == name).FirstOrDefault()!;
			if (data != null)
			{
				return true;
			}
			return false;
		}

		[HttpPost("Save")]
        public VMResponse Save(TblRole data)
        {
            data.CreatedBy = IdUser;
            data.CreatedDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                response.Message = "Data success saved";
            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = "Failed saved " + e.Message;
            }
            return response;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblRole data)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == data.Id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.RoleName = data.RoleName;
                dt.UpdatedBy = IdUser;
                dt.UpdatedDate = DateTime.Now;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    response.Message = "Data success updates";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Update failed " + e.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }

        [HttpDelete("Delete/{id}/{createBy}")]
        public VMResponse Delete(int id)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdatedBy = IdUser;
                dt.UpdatedDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    response.Message = $"Data {dt.RoleName} success deleted";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Delete Failed " + e.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }
    }
}
