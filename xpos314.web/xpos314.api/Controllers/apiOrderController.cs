﻿using System.Globalization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiOrderController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private VMResponse response = new VMResponse();
        private int IdUser = 1;
        
        public apiOrderController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetDataOrderHeader")]
        public List<TblOrderHeader> GetDataOrderHeader()
        {
            List<TblOrderHeader> data = db.TblOrderHeaders.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataOrderDetail/{id}")]
        public List<VMTblOrderDetail> GetDataOrderDetail(int id)
        {
            List<VMTblOrderDetail> data = (from d in db.TblOrderDetails
                                         join p in db.TblProducts on d.IdProduct equals p.Id
                                         where d.IsDelete == false && d.IdHeader == id
                                         select new VMTblOrderDetail
                                         {
                                             Id = d.Id,
                                             Qty = d.Qty,
                                             SumPrice = d.SumPrice,

                                             IdProduct = d.IdProduct,
                                             NameProduct = p.NameProduct,
                                             Price = p.Price
                                         }).ToList();
            return data;
        }
        [HttpPost("SubmitOrder")]
        public VMResponse SubmitOrder(VMTblOrderHeader dataHeader)
        {
            TblOrderHeader head = new TblOrderHeader();
            head.CodeTransaction = GenerateCode();
            head.Amount = dataHeader.Amount;
            head.TotalQty = dataHeader.TotalQty;
            head.IdCustomer = IdUser;
            head.IsCheckout = true;
            head.IsDelete = false;
            head.CreateBy = IdUser;
            head.CreateDate = DateTime.Now;

            try
            {
                db.Add(head);
                db.SaveChanges();

                foreach(VMTblOrderDetail item in dataHeader.ListDetails)
                {
                    TblOrderDetail detail = new TblOrderDetail();
                    detail.IdHeader = head.Id;
                    detail.IdProduct = item.IdProduct;
                    detail.Qty = item.Qty;
                    detail.SumPrice = item.SumPrice;
                    detail.IsDelete = false;
                    detail.CreateBy = IdUser;
                    detail.CreateDate = DateTime.Now;
                    

                    try
                    {
                        db.Add(detail);
                        db.SaveChanges();

                        TblProduct dataProduct = db.TblProducts.Where(a => a.Id == item.IdProduct).FirstOrDefault()!;

                        if(dataProduct != null)
                        {
                            dataProduct.Stock -= item.Qty;

                            db.Update(dataProduct);
                            db.SaveChanges();
                        }

                        response.Message = "Thanks for  order";
                    }
                    catch(Exception ex)
                    {
                        response.Success = false;
                        response.Message = "Failed saved : " + ex.Message;
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = "Failed saved : " + e.Message;
            }
            return response;
        }

        [HttpGet("GenerateCode")]
        public string GenerateCode()
        {
            string code = $"XPOS-{DateTime.Now.ToString("yyyyMMdd")}-";
            string digit = "";

            TblOrderHeader data = db.TblOrderHeaders.OrderByDescending(a => a.CodeTransaction).FirstOrDefault()!;
            if (data != null)
            {
                string codeLast = data.CodeTransaction;
                string[] codeSplit = codeLast.Split("-");
                int intLast = int.Parse(codeSplit[2]) + 1;
                digit = intLast.ToString("00000");
            }
            else
                digit = "00001";

            string codetrans = code + digit;
            return codetrans;
        }

        [HttpGet("CountTransaction/{IdCustomer}")]
        public int CountTransaction(int IdCustomer)
        {
            int count = 0;
            count = db.TblOrderHeaders.Where(a => a.IsDelete == false && 
                                                  a.IdCustomer == IdCustomer).Count();
            return count;
        }

        [HttpGet("GetDataOrderHeaderDetail/{IdCustomer}")]
        public List<VMTblOrderHeader> GetDataOrderHeaderDetail(int IdCustomer)
        {
           /* DateTime dt = DateTime.Now;
            var hari = dt.ToString("dddd", new CultureInfo("id-ID"));
            var jam = dt.Hour;
            var menit = dt.Minute;*/

            List<VMTblOrderHeader> data = (from h in db.TblOrderHeaders
                                           where h.IsDelete == false && h.CreateBy == IdCustomer
                                           select new VMTblOrderHeader
                                           {
                                               Id = h.Id,
                                               Amount = h.Amount,
                                               TotalQty = h.TotalQty,
                                               IsCheckout = h.IsCheckout,
                                               IdCustomer = h.IdCustomer,
                                               CodeTransaction = h.CodeTransaction,
                                               CreateDate = h.CreateDate,
                                               ListDetails = (from d in db.TblOrderDetails
                                                              join p in db.TblProducts on d.IdProduct equals p.Id
                                                              where d.IsDelete == false && d.IdHeader == h.Id
                                                              select new VMTblOrderDetail
                                                              {
                                                                  Id = d.Id,
                                                                  Qty = d.Qty,
                                                                  SumPrice = d.SumPrice,
                                                                  IdHeader = d.IdHeader,
                                                                  IdProduct = d.IdProduct,
                                                                  NameProduct = p.NameProduct,
                                                                  Price = p.Price
                                                              }).ToList(),
                                               /*online = h.Id == 2 && 
                                                        int.Parse(h.CodeTransaction.Substring(0,2)) > jam &&
                                                        int.Parse(h.CodeTransaction.Substring(0,2)) > menit?true:false*/
                                           }).ToList();
            return data;
        }

    }
}
