create database XPOS_314
use XPOS_314

create table TblCategory
(
id int primary key identity(1,1) not null,
NameCategory varchar(50) not null,
Description varchar(100),
isDelete bit,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int,
UpdateDate datetime
)

select * from TblCategory

delete from TblCategory where id = 3


--untuk hapus semua data pada tabel
truncate table TblCategory


create table TblVariant
(
	id int primary key identity(1,1) not null,
	IdCategory int not null,
	NameVariant varchar(50) not null,
	Description varchar(100) null,
	IsDelete bit null,
	CreateBy int not null,
	CreateDate datetime not null,
	UpdateBy int null,
	UpdateDate datetime null
)

insert into TblVariant(IdCategory,NameVariant,IsDelete,Description,CreateBy,CreateDate)
values
(1, 'Boba Sundae', 0,'Ice Cream',1,'2023/03/31')

select * from TblVariant
select * from TblCategory

truncate table TblVariant

create table TblProduct
(
	id int primary key identity(1,1) not null,
	IdVariant int not null,
	NameProduct varchar(100) not null,
	Price decimal(18,0) not null,
	Stock int not null,
	Image varchar(100),
	IsDelete bit,
	CreateBy int not null,
	CreateDate datetime not null,
	UpdateBy int,
	UpdateDate datetime
)

select * from TblVariant
select * from TblCategory
select * from TblProduct
truncate table TblProduct

insert into TblProduct
values (3,'Mango Smoothies',30000,22,'mango smoothies.jpg',0,1,GETDATE(),null,null)

delete from TblProduct where NameProduct='Milo Mille Crepes'


create table TblOrderHeader
(
Id int primary key identity(1,1) not null,
CodeTransaction nvarchar(20) not null,
IdCustomer int not null,
Amount decimal(18,2) not null,
TotalQty int not null,
IsCheckout bit not null,
IsDelete bit,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int,
UpdateDate datetime
)

create table TblOrderDetail
(
Id int primary key identity not null,
IdHeader int not null,
IdProduct int not null,
Qty int not null,
SumPrice decimal(18,2) not null,
IsDelete bit,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int,
UpdateDate datetime
)

select * from TblOrderDetail
select * from TblOrderHeader
select * from TblProduct

drop table TblOrderHeader

truncate table TblOrderHeader
truncate table TblOrderDetail

create table TblRole
(
	Id int primary key identity(1,1) not null,
	RoleName varchar(80),
	IsDelete bit null,
	CreatedBy int not null,
	CreatedDate datetime not null,
	UpdatedBy int,
	UpdatedDate datetime null
)

create table TblCustomer
(
	Id int primary key identity not null,
	NameCustomer nvarchar(50) not null,
	Email nvarchar(100) not null,
	Password nvarchar(100) not null,
	Address nvarchar(max) not null,
	Phone nvarchar(15) not null,
	IdRole int,
	IsDelete bit not null,
	CreateBy int not null,
	CreateDate datetime not null,
	UpdateBy int,
	UpdateDate datetime
)


select * from TblCustomer
select * from TblRole
select * from TblMenu
select * from TblMenuAccess


truncate table TblCustomer


insert into TblMenuAccess (IdRole,MenuId,IsDelete,CreatedBy,CreatedDate)
values
(1,2,0,1,GETDATE())

delete from TblCustomer where Id=3